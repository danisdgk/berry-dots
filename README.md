# berry-dots

My Berry WM dotfiles

## Screenshots

### Cava
![cava](screenshots/cava.png)

### Rxfetch
![rxfetch](screenshots/rxfetch.png)

### Rofi
![rofi](screenshots/rofi.png)

### Polybar launcher in the top left
![launcher](screenshots/launcher.png)

### Power Menu
![powermenu](screenshots/powermenu.png)

## Requirements

* Window Manager: [berry](https://github.com/JLErvin/berry)
* Program Launcher: [rofi](https://github.com/davatorium/rofi)
* Bar: [polybar](https://github.com/polybar/polybar)

### Optional:

* Audio Visualizer: [cava](https://github.com/karlstav/cava)
* Fetch: [rxfetch](https://github.com/Mangeshrex/rxfetch)

#### Credit
The bar and rofi theme are both originally from adi1090x' [polybar-themes](https://github.com/adi1090x/polybar-themes#shades), but edited by me
